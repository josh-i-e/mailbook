from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
from django.template import loader
from django.shortcuts import get_object_or_404, render
from django.shortcuts import redirect

from .models import Contacts
from .forms import ContactForm

def index(request):
    return render(request, 'mailbook/index.html')
	

def list(request):
	contact_objects = Contacts.objects.all().order_by('-id')
	context = {'contacts': contact_objects}
	return render(request, 'mailbook/list.html', context)
	
	
def create(request):
	if request.method == "POST":
		form = ContactForm(request.POST)
		if form.is_valid():
			contact = form.save(commit=False)
			contact.first_name = form.cleaned_data['first_name']
			contact.last_name = form.cleaned_data['last_name']
			contact.email = form.cleaned_data['email']
			contact.save()
			context = {
				'fname': contact.first_name,
				'lname': contact.last_name,
				'email': contact.email
			}
			return redirect('/list')
	else:
		form = ContactForm()
	return render(request, 'mailbook/add.html', {'form': form})
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
