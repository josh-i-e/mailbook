# README #

mailbook is a Django application which stores names and email addresses in a SQLite database. 

* The home page (welcome page) is located at http://localhost:[port]/ - this page has links to list and add pages 
* The list page displays all stored names/email address. located at http://localhost:[port]/list 
* The add page displays a form that Adds name / email addresses to the database. Located at http://localhost:[port]/add This page validates input and shows errors(if any).

### Requirements ###
* Django==1.11.7
* pkg-resources==0.0.0
* pytz==2017.3


### Set up ###

* Clone the repository into a folder on your computer. 
* Install requirements. pip install requirements.txt
* Run the command 'python manage.py runserver' in a command line/terminal from the src folder to start the server. 
* Run the command 'python manage.py migrate' to setup the database tables. 
* Visit http://localhost:[port] or http://127.0.0.1:[port] to access the application index page.

### Assumptions ###

* Duplicate emails are not allowed to be inserted into the database
* Duplicate first and last name are allowed (no unique constraint)
* The records on the list page appears in descending order (newest first)
* The app is in dev mode, thus Debug was set to True

### Security ###

XSS Prevention: No html was stored in the db as the form input are validated and cleaned.  The application properly escapes user-provided data before it is placed on the page.  The Django template in use ensures expressions are HTML-escaped, and it'll automatically escape data that's dynamically inserted into the template.   The use of mark_safe and safe tags in the templates were avoided.

Sql Injection: By using Djangoâ€™s querysets and(ORM) layer as against writing RawSQL queries and making extra calls, the resulting SQL were properly escaped by the sqlite3 database driver used.  The use of is_valid() helps to generate validated form data in its cleaned_data attribute.

CSRF: The CSRF middleware is left active in the MIDDLEWARE setting.  The CSRF module is enabled for All views (globally).  In the template that uses a POST form, the csrf_tokens tag is inserted inside the element.  In the corresponding view functions of the form action pages, the RequestContext is used to render the response so that {% csrf_token %} works properly.  The usage of render() function, protects the app since it uses RequestContext.

The X-Frame-Options header is left at its default value of SAMEORIGIN for every outgoing HttpResponse.  This way, if the response contains the header with a value of SAMEORIGIN then the browser will only load the resource in a frame if the request originated from application.

The 'django.middleware.clickjacking.XFrameOptionsMiddleware' was set in MIDDLEWARE. This sets the same X-Frame-Options value for all responses in the application.

Optional use of the package django-secure, can help to configure and check some security aspects such as ssl.

